package wp.tutoriais.imagemmysql;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {
    //Criar um objeto para o ImageView
    ImageView verImagem;
    //Objeto para armazenar a imagem em bytes
    byte[] imagemEmByte;
    //Objeto para armazenar a imagem em String no padrão Base64
    String imagemEmString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Criação de objeto para o botão
        Button btImagem = findViewById(R.id.btCarregarImagem);
        //Iniciando o objeto do ImageView
        verImagem = findViewById(R.id.verImagem);

        btImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Criação de uma Intent para abrir a câmera e tirar foto
                Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                resultadoIntent.launch(camera_intent);
            }
        });
    }

    //Configurando o método de retorno que será acionado assim que a foto for tirada e escolhida
    ActivityResultLauncher<Intent> resultadoIntent = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        //Teste para verificar se há algo retornando da câmera, ou seja, se há foto
        if (result.getResultCode() == RESULT_OK) {
            //Recuperando os dados enviados pela câmera
            Bundle extras = result.getData().getExtras();
            //De todos os dados acima, filtrando para pegar o "data" que é aonde a imagem está
            Bitmap imagemEmBitmap = (Bitmap) extras.get("data");
            //Colocar a imagem dentro do ImageView
            verImagem.setImageBitmap(imagemEmBitmap);

            //Objeto para poder converter a imagem em vetor de byte
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //Populando o "stream" com o conteúdo da imagem
            imagemEmBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            //Convertendo o "stream" para um vetor de byte
            imagemEmByte = stream.toByteArray();
            //Converter o vetor de byte para Base64
            imagemEmString = Base64.encodeToString(imagemEmByte, Base64.DEFAULT);

        }
    });

    //Método para criar uma requisição ao webservice e enviar dados ao método
    //que fará o cadastro da imagem no banco de dados
    private void cadastrarImagemBD(){
        //Criar um objeto da classe Volley para configurar as requisições ao webservice
        RequestQueue solicitacao = Volley.newRequestQueue(this);
        //Configuração do endpoint (url) da requisição
        String url = "http://10.0.2.2:5000/api/Imagem/";

        //Configurar os dados que serão enviados para o método no webservice
        JSONObject enviar = new JSONObject();
        try {
            enviar.put("id", 0); //Autoincremento pelo bando de dados
            enviar.put("imagemByte", imagemEmByte);
            enviar.put("imagemTexto", imagemEmString);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Configurando a requisição a ser enviada
        JsonObjectRequest envio = new JsonObjectRequest(Request.Method.POST,
                url, enviar, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(response.has("id")){ //Se o webservice retornar algo que tenha "id"
                    //Exibe mensagem na tela para confirmar o cadastro
                    Snackbar.make(MainActivity.this, findViewById(R.id.tela), "Cadastrado", Snackbar.LENGTH_SHORT).show();
                }else{
                    //Exibe mensagem na tela para informar que não foi possível cadastrar
                    Snackbar.make(MainActivity.this, findViewById(R.id.tela), "Cadastrado", Snackbar.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Erro ao conectar", Toast.LENGTH_SHORT).show();
                Log.d("APP_MYSQL", ">>>>>>>>>>>>>>>>> " + error.getMessage());
            }
        });

        solicitacao.add(envio);
    }
}